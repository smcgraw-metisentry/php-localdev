# Local PHP Development Environment
## Uses Docker and Docker Compose to create a local dev environment
### Uses php v5.6 and optional mysql v5.7 (uncomment the mysql service)
### Usage:
- place the docker compose file in your top level directory
- change the webroot and bind mount as needed
- bring up the container environment: ```docker-compose up -d```
- create index.php (if needed) with the following to verify:
    ```php
    <?php phpinfo(); ?>
    ```
- point your browser to [http://localhost:8080](http://localhost:8080)
### To use MySQL:
- create `etc` directory and `environment.yml`
    ```bash
    mkdir etc && touch etc/enveronment.yml
    ```
- add and adjust the following in `environment.yml` for mysql
    ```
    MYSQL_ROOT_PASSWORD=dev
    MYSQL_USER=dev
    MYSQL_PASSWORD=dev
    MYSQL_DATABASE=database
    ```
- uncomment the mysql service and volumes at the bottom of the docker-compose file